package com.avenedra.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.avenedra.R;

import java.io.IOException;
import java.io.InputStream;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Pritam on 28-06-2016.
 */
public class LoginActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    @Bind(R.id.btn_sign_in)
    Button btn_login;

    @Bind(R.id.spinner)
    Spinner spinner;

    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        spinner.setOnItemSelectedListener(this);

    }

    @OnClick(R.id.btn_sign_in)
    void getLogin()
    {
        String selected_value = spinner.getSelectedItem().toString();
        Toast.makeText(LoginActivity.this,"selected item"+selected_value,Toast.LENGTH_SHORT).show();

        Intent intent = new Intent(LoginActivity.this,Courses_Asyn_Activity.class);
        startActivity(intent);
    }

    public String loadJSONFromAsset() {
        String json = null;
        try {

            InputStream is = getAssets().open("login.json");

            int size = is.available();

            byte[] buffer = new byte[size];

            is.read(buffer);

            is.close();

            json = new String(buffer, "UTF-8");


        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        String item_name = parent.getItemAtPosition(position).toString();

        Toast.makeText(LoginActivity.this,""+item_name,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
